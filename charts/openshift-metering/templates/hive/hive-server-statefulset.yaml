apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: hive-server
  labels:
    app: hive
    hive: server
spec:
  serviceName: hive-server
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  selector:
    matchLabels:
      app: hive
      hive: server
{{- if .Values.hive.spec.labels }}
{{ toYaml .Values.hive.spec.labels | indent 6 }}
{{- end }}
  template:
    metadata:
      labels:
        app: hive
        hive: server
{{- if .Values.hive.spec.labels }}
{{ toYaml .Values.hive.spec.labels | indent 8 }}
{{- end }}
      annotations:
        hive-configmap-hash: {{ include (print $.Template.BasePath "/hive/hive-configmap.yaml") . | sha256sum }}
        hive-scripts-hash: {{ include (print $.Template.BasePath "/hive/hive-scripts-configmap.yaml") . | sha256sum }}
        hive-jmx-config-hash: {{ include (print $.Template.BasePath "/hive/hive-jmx-config.yaml") . | sha256sum }}
        hive-aws-credentials-secret-hash: {{ include (print $.Template.BasePath "/hive/hive-aws-credentials-secret.yaml") . | sha256sum }}
{{- if .Values.hive.spec.annotations }}
{{ toYaml .Values.hive.spec.annotations | indent 8 }}
{{- end }}
    spec:
      securityContext:
{{ toYaml .Values.hive.spec.securityContext | indent 8 }}
{{- if .Values.hive.spec.server.nodeSelector }}
      nodeSelector:
{{ toYaml .Values.hive.spec.server.nodeSelector | indent 8 }}
{{- end }}
{{- if .Values.hive.spec.server.tolerations }}
      tolerations:
{{ toYaml .Values.hive.spec.server.tolerations | indent 8 }}
{{- end }}
{{- if .Values.hive.spec.server.affinity }}
      affinity:
{{ toYaml .Values.hive.spec.server.affinity | indent 8 }}
{{- end }}
      containers:
      - name: hiveserver2
        command: ["/hive-scripts/entrypoint.sh"]
        args: ["/opt/hive/bin/hive", "--service", "hiveserver2"]
        image: "{{ .Values.hive.spec.image.repository }}:{{ .Values.hive.spec.image.tag }}"
        imagePullPolicy: {{ .Values.hive.spec.image.pullPolicy }}
        ports:
        - name: thrift
          containerPort: 10000
          protocol: TCP
        - name: ui
          containerPort: 10002
          protocol: TCP
        - containerPort: 8082
          name: metrics
{{- if .Values.hive.spec.server.readinessProbe }}
        readinessProbe:
{{ toYaml .Values.hive.spec.server.readinessProbe | indent 10 }}
{{- end }}
{{- if .Values.hive.spec.server.livenessProbe }}
        livenessProbe:
{{ toYaml .Values.hive.spec.server.livenessProbe | indent 10 }}
{{- end }}
        terminationMessagePath: /dev/termination-log
        env:
        - name: HIVE_LOGLEVEL
          value: {{ upper .Values.hive.spec.server.config.logLevel | quote}}
{{- if .Values.hive.spec.server.config.jvm.initialRAMPercentage }}
        - name: JVM_INITIAL_RAM_PERCENTAGE
          value: "{{ .Values.hive.spec.server.config.jvm.initialRAMPercentage }}"
{{- end }}
{{- if .Values.hive.spec.server.config.jvm.maxRAMPercentage }}
        - name: JVM_MAX_RAM_PERCENTAGE
          value: "{{ .Values.hive.spec.server.config.jvm.maxRAMPercentage }}"
{{- end }}
{{- if .Values.hive.spec.server.config.jvm.maxRAMPercentage }}
        - name: JVM_MIN_RAM_PERCENTAGE
          value: "{{ .Values.hive.spec.server.config.jvm.minRAMPercentage }}"
{{- end }}
{{ include "hive-env" . | indent 8 }}
        - name: MY_MEM_REQUEST
          valueFrom:
            resourceFieldRef:
              containerName: hiveserver2
              resource: requests.memory
        - name: MY_MEM_LIMIT
          valueFrom:
            resourceFieldRef:
              containerName: hiveserver2
              resource: limits.memory
        volumeMounts:
        - name: hive-config
          mountPath: /hive-config
        - name: hive-scripts
          mountPath: /hive-scripts
{{- if .Values.hive.spec.config.useHadoopConfig }}
        - name: hadoop-config
          mountPath: /hadoop-config
{{- end}}
        - name: hive-jmx-config
          mountPath: /opt/jmx_exporter/config
        # openshift requires volumeMounts for VOLUMEs in a Dockerfile
        - name: hive-metastore-db-data
          mountPath: /var/lib/hive
        - name: namenode-empty
          mountPath: /hadoop/dfs/name
        - name: datanode-empty
          mountPath: /hadoop/dfs/data
        - name: hadoop-logs
          mountPath: /opt/hadoop/logs
{{- if .Values.hive.spec.config.sharedVolume.enabled }}
        - name: hive-warehouse-data
          mountPath: {{ .Values.hive.spec.config.sharedVolume.mountPath }}
{{- end }}
        resources:
{{ toYaml .Values.hive.spec.server.resources | indent 10 }}
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      terminationGracePeriodSeconds: {{ .Values.hive.spec.terminationGracePeriodSeconds }}
      serviceAccount: hive
{{- if .Values.hive.spec.imagePullSecrets }}
      imagePullSecrets:
{{ toYaml .Values.hive.spec.imagePullSecrets | indent 8 }}
{{- end }}
      volumes:
      - name: hive-config
        configMap:
          name: hive-config
      - name: hive-scripts
        configMap:
          name: hive-scripts
          defaultMode: 0555
{{- if .Values.hive.spec.config.useHadoopConfig }}
      - name: hadoop-config
        secret:
          secretName: {{ .Values.hive.spec.config.hadoopConfigSecretName }}
{{- end }}
      - name: hive-jmx-config
        configMap:
          name: hive-jmx-config
      # these emptyDir volumes are necessary because Openshift requires VOLUMEs
      # in a Dockerfile have a corresponding volumeMount
      - name: namenode-empty
        emptyDir: {}
      - name: datanode-empty
        emptyDir: {}
      - name: hive-metastore-db-data
        emptyDir: {}
      - name: hadoop-logs
        emptyDir: {}
{{- if .Values.hive.spec.config.sharedVolume.enabled }}
      - name: hive-warehouse-data
        persistentVolumeClaim:
          claimName: {{ .Values.hive.spec.config.sharedVolume.claimName }}
{{- end}}
